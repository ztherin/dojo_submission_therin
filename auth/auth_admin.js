const jwebtoken = require( 'jsonwebtoken' ) ;

module.exports = ( req , res , next ) => {
    try{
        let adminid = null ;
        let token = null ;

        if( req.session.token && req.session.admin_id ){
            token = req.session.token ;
            adminid = req.session.admin_id ;
        }

        else if( req.headers.token && req.headers.adminid ){
            const d_token = req.headers.token.split( ' ' ) ;
            token = d_token == 2 ? d_token[ 1 ] : d_token[ 0 ] ;
            
            adminid = req.headers.adminid ;
        }

        else if (req.params.id && req.params.token) {
			token   = request.params.token;
			adminid = request.params.id;
		}

        if( token && adminid ) {
			// on decode le token :
			const dectoken = jwebtoken.verify(token, 'iTfynMtR2CA9xbFRzdcrnvGGN_LIiXlT4PVpJk8VZnjbrbPoS3HjgNvBa73sGyUIqsLkhwp4cqqnue7Y2CsngA');

			// on extrait les l'ID utilisateur du token decode :
			const uid = dectoken.admin_id ;

			// on fait une verification :
			if (adminid === uid)
				next();

			else 
				throw "Invalid user ID";
		}
		else 
			throw "Invalid user ID";
    }
    catch{
        console.log( 'Fail of authentication !' ) ;
        res.status( 401 ).json({
            message : "Fail of authentication"
        });
    }
}
const express = require( 'express' ) ;
const adminCtrl = require( '../controllers/admin' ) ;

const router = express.Router() ;

router.post( '/login_auth' , adminCtrl.loginAuth ) ;

module.exports = router ;
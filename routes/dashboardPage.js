const express = require( 'express' ) ;
const router = express.Router() ;
const dashboardPageCtrl = require( '../controllers/dashboardPage' ) ;
var auth = require( "../auth/auth_admin" ) ;

router.get( '/' , auth ,dashboardPageCtrl.dashboardPage ) ;

module.exports = router ;
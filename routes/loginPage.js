const express = require( 'express' ) ;
const router = express.Router() ;
const loginPageCtrl = require( '../controllers/loginPage' ) ;

router.get( '/' , loginPageCtrl.loginPage ) ;

module.exports = router ;
const express = require( 'express' ) ;
const router = express.Router() ;
const submitCtrl = require( '../controllers/submission' ) ;

const auth = require( '../auth/auth_admin' ) ;

router.post( '/create' , submitCtrl.createSubmission ) ;
router.get( '/get/:id' , auth , submitCtrl.getOneSubmission ) ;
router.get( '/get/all/:status' , auth , submitCtrl.getAllSubmission ) ;
router.get( '/count/:status' , auth , submitCtrl.countOneSubmission ) ;
router.get( '/counts' , submitCtrl.countAllSubmission ) ;
router.put( '/update/:id' , auth , submitCtrl.modifySubmission ) ;
router.delete( '/delete/:id' , auth , submitCtrl.deleteSubmission ) ;

module.exports = router ;
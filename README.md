# dojo_submission_therin

<!-- GETTING STARTED -->
## Commencer 

### prérequis

* nodejs
* npm

### Installation
# Aller dans le dossier backend se situant à la racine et lancer la commande :
```sh
	npm install
```

## Usage

1- Aller dans le dossier backend se situant à la racine et lancer la commande :
```sh
	node server.js
```

2- Ouvrir un navigateur et entrer dans la barre de recherche l'adresse suivante :
	-Pour la page de soumission de formulaire pour le dojo.
```text
	http://127.0.0.1:3000
```
	-Pour la page de login de l'administracteur
```text
	http://127.0.0.1:3000/login/
``` 
		Voici les informations Admin :
		`User name : dojoAdmin3d48`
		`password : nexAmdncomputer$63d48`


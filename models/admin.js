const mongoose = require( 'mongoose' ) ;

const uniqueValidator = require( 'mongoose-unique-validator' ) ;

const admin_schema = mongoose.Schema({
    user_name : { type : String , required : true , unique : true },
    password : { type : String , required : true } ,
    token : { type : String , required : true }
});

admin_schema.plugin( uniqueValidator ) ;

module.exports = mongoose.model( 'Admin' , admin_schema ) ;
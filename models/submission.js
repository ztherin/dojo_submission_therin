const mongoose = require( 'mongoose' ) ;

const submissionSchema = mongoose.Schema({
    firstName : { type : String, required : true },
    lastName : { type : String , required : true },
    age : { type : Number , required : true } ,
    experience : { type : String , required : true } ,
    motivation : { type : String , required : true } ,
    status : { type : Number , required : true } ,
    created : { type : Date , required : true } ,
    updated : { type : Date , required : true }
});

module.exports = mongoose.model( 'submission' , submissionSchema ) ;
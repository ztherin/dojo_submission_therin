const bcrypt = require( 'bcrypt' ) ;
const Admin = require( '../models/admin' ) ;
const jwebtoken = require( 'jsonwebtoken' ) ;
const { replaceOne } = require('../models/admin');

exports.loginAuth = ( req, res, next ) => {
    let error = false ;

    switch( undefined ){
        case req.body.user_name : error = true ; break ;
        case req.body.password : error = true ; break ;
    }

    if( error ){
        res.status( 401 ).json({ message : 'Username or password is not valid !' }) ;
        return false ;
    }

    Admin.findOne({ user_name : req.body.user_name })
        .then( admin => {
            if( admin ){
                bcrypt.compare( req.body.password, admin.password )
                    .then( valid => {

                        if( valid ){
                            req.session.token = jwebtoken.sign({
                                admin_id : admin._id },
                                'iTfynMtR2CA9xbFRzdcrnvGGN_LIiXlT4PVpJk8VZnjbrbPoS3HjgNvBa73sGyUIqsLkhwp4cqqnue7Y2CsngA',
                                {expiresIn : '1h' }
                            );
                            req.session.admin_id = admin._id ;

                            res.status( 200 ).json({
                                message : "login succefully",
                                admin_id : admin._id,
                                token : req.session.token
                            });

                            //req.session.connected = true ;

                            //console.log( req.session ) ;
                        }
                        
                        else
                            return res.status( 401 ).json({ message : 'Username or password is not valid !' }) ;
                    })
                    .catch( error => res.status( 500 ).json({ message : 'Error of authentication in server '}) ) ;
            }

            else
                return res.status( 401 ).json({ message : 'Username or password is not valid !' }) ;
        })
        .catch( error => res.status( 500 ).json({ message : 'Error of authentication in server '}) ) ;
}
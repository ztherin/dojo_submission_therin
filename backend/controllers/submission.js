const { replaceOne } = require("../models/submission");
const Submission = require( "../models/submission" ) ;

exports.createSubmission= ( req, res, next ) => {
    const submission = new Submission({
        ...req.body ,
        created : new Date() ,
        updated : new Date()
    });

    console.log( submission ) ;

    submission.save()
        .then( () => res.status( 201 ).json({ message : 'Submission created !' }))
        .catch( error => res.status( 400 ).json({ error : 'Submission faled !' })) ;
};

exports.getOneSubmission = ( req, res, next ) =>{
    Submission.findOne({ _id: req.params.id })
      .then( submission => res.status( 200 ).json( submission ))
      .catch( error => res.status( 404 ).json({ error })) ;
};

exports.getAllSubmission = ( req, res, next ) =>{
    
    Submission.find({ status : req.params.status })
      .then( submission => res.status( 200 ).json( submission ))
      .catch( error => res.status( 400 ).json({ error })) ;
};

exports.countOneSubmission = ( req, res, next ) =>{
    
  Submission.find({ status : req.params.status }).count()
    .then( number => res.status( 200 ).json( number ))
    .catch( error => res.status( 400 ).json({ error })) ;
};

exports.countAllSubmission = ( req, res, next ) =>{
  let tabCount = [] ;
  Submission.find({ status : 0 }).count()
    
    .then( number => {
      tabCount[0] = number ;
      Submission.find({ status : 1 }).count()
        
        .then( number => {
          tabCount[1] = number ;
          Submission.find({ status : 2 }).count()
            
            .then( number => {
              tabCount[2] = number ;
              res.status( 200 ).json( tabCount ) ;
            })
            .catch( error => res.status( 401 ).json({ error })) ;
        })
        .catch( error => res.status( 402 ).json({ error })) ;
    })
    .catch( error => res.status( 403 ).json({ error })) ;
};

/*exports.tsts = ( req, res, next ) =>{
  Submission.findOne({ _id : req.params.id })
    .then( sbm => {
      sbm.firstName = "Merca" ;
      console.log( sbm ) ;
      Submission.updateOne({ _id: req.params.id } , { firstName : sbm.firstName , _id: req.params.id })
        .then( () => res.status( 200 ).json( {message : "Objet updated"} ))
        .catch( error => res.status( 404 ).json({ error })) ;
    })
    .catch( error => res.status( 403 ).json({ error })) ;
};*/

exports.modifySubmission = ( req, res, next ) =>{
    let error = false ;

    switch( undefined ){
      case req.body.status : error = true ; break ;
      default : break ;
    }

    if( error ){
      res.status( 400 ).json({ message : "Arguments missing !" }) ;
      return false ;
    }

    error = typeof req.body.status != 'number' ;

    error = error || ( req.body.status < 1 || req.body.status > 2 ) ;

    if( error ){
      res.status( 400 ).json({ message : "Wrong arguments!" }) ;
      return false ;
    }

    req.body.updated = new Date() ;
    Submission.updateOne({ _id: req.params.id } , { ...req.body , _id: req.params.id })
      .then( () => res.status( 200 ).json( {message : "Objet updated"} ))
      .catch( error => res.status( 404 ).json({ error })) ;
};

exports.deleteSubmission = ( req, res, next ) =>{
    Submission.deleteOne({ _id: req.params.id })
      .then( things => res.status( 200 ).json( {message:"object delected"} ))
      .catch( error => res.status( 404 ).json({ error })) ;
};


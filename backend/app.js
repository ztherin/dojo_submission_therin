const express = require( 'express' ) ;
const mongoose = require( 'mongoose' ) ;
const bodyParser = require( 'body-parser' ) ;

const session = require('express-session')

const indexRoute = require( './routes/index' ) ;
const loginPageRoute = require( './routes/loginPage' ) ;
const dashboardPageRoute = require( './routes/dashboardPage' ) ;

const submitRout = require( './routes/submission' ) ;
const adminRoute = require( './routes/admin' ) ;

const app = express() ;

/*
    se connecter a la base de donner grace a mongoose.
*/
mongoose.connect( 'mongodb+srv://none:therine3d48@cluster0.3kpg3.mongodb.net/dojo_submit_db?retryWrites=true&w=majority' ,
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
).then( ()=> console.log( 'Connexion to MongoDB succefully !' ) )
 .catch( ()=> console.log( 'Connexion to MongoDB falled !' ) ) ;

 /*
  Definir les header a l'objet reponse 
  qu'on renvois au navigateur pour dire tout vas bien,
  vous pouvez utiliser cet API.
*/

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use( bodyParser.json() );

app.use(session({
    secret: 'TAF0TvQfEj6BpJmjhKQ7-JatgB-bzmJzkFAtXWI3UhKJliVfbZOFlfucUdV3HsTgJkhRNHPKaSnqn-qQTqf1Dw',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
})) ;

/* Pour donner acces au dossier de frontend */
app.use( express.static( "../frontend" ) ) ;

app.use( '/', indexRoute ) ;
app.use( '/submission', submitRout ) ;
app.use( '/admin' , adminRoute ) ;
app.use( '/login' , loginPageRoute ) ;
app.use( '/dashboard' , dashboardPageRoute ) ;

module.exports = app ;
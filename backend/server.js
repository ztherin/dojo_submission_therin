//mporter le package HTTP natif de Node
const { Console } = require('console');
const http = require( 'http' ) ;
const { type } = require('os');
const { listen } = require('./app');

//###################################################################################
/* 
  importer le app.js
*/
const app = require( './app' ) ;

//###################################################################################
/*
  créer un serveur,
  en passant une fonction qui sera exécutée à chaque appel effectué vers ce serveur.
  Cette fonction reçoit les objets request et response en tant qu'arguments. 
  Dans cet exemple, vous utilisez la méthode end de la réponse pour renvoyer 
  une réponse de type string à l'appelant.
*/
/*
  
  const server = http.createServer( (req, res) => {
    res.end( 'Voila la reponse du serveur est celle ci!' ) ;
  });

*/

//##################################################################################
/*
  la fonction normalizePort renvoie un port valide,
  qu'il soit fourni sous la forme d'un numéro ou d'une chaîne ;
*/
const normalizePort = val => {
  const port = parseInt( val, 10 ) ;

  if( isNaN( port ) ){
    return val ;
  }

  if( port >= 0 ){
    return port ;
  }

  return false ;
};

//##################################################################################
/*
  l'application qui est creer dans express 
  est une fonnction qui va recevoir la requete et la reponse 
*/
const port = normalizePort( process.env.PORT || 3000 ) ;
app.set( 'port' , port ) ;


//#################################################################################
/*
  la fonction errorHandler  recherche les différentes
  erreurs et les gère de manière appropriée.
  Elle est ensuite enregistrée dans le serveur ;
*/
const errorHandler = error =>{
  if( error.syscall !== 'listen' ){
    throw error ;
  }

  const address = server.address() ;
  const bind = typeof address === 'string' ? 'pipe' + address : 'port: ' + port ;

  switch( error.code ){
    case 'EACCES' :
      console.error( bind + ' requires elevated privileges.' ) ;
      process.exit( 1 ) ;
      break ;
    
    case 'EADDRINUSE' :
      console.error( bind + ' is already in use.' ) ;
      process.exit( 1 ) ;
      break ;

    default :
    throw error ;
  }
};

const server = http.createServer( app ) ;
server.on( 'error' , errorHandler ) ;

//##################################################################################
/*
  un écouteur d'évènements est également enregistré,
  consignant le port ou le canal nommé 
  sur lequel le serveur s'exécute dans la console.
*/
server.on( 'listening' , () => {
  const address = server.address() ;
  const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + port ;
  console.log( 'Listening on ' + bind ) ;
});


//##################################################################################
/*
  Dans la dernière ligne, vous configurez le serveur pour qu'il écoute :

    -soit la variable d'environnement du port grâce à process.env.PORT : 
      -si la plateforme de déploiement propose un port par défaut,
      c'est celui-ci qu'on écoutera ;

    -soit le port 3000, 
      ce qui nous servira dans le cas de notre plateforme de développement.
*/
server.listen( port ) ;
var data ;
var xhr = new XMLHttpRequest() ;
xhr.withCredentials = true;

$( '#formSubmit' ).click( function( e ){

    let firstName = String( $( '#firstName' ).val() ) ;
    let lastName = String( $( '#lastName' ).val() ) ;
    let age = Number( $( '#age' ).val() ) ;
    let experience = String( $( '#experience' ).val() ) ;
    let motivation = String( $( '#motivation' ).val() ) ;
    
    if( firstName!='' && lastName!='' && age>=5 && experience!='' && motivation!='' ){
        data = {
            "firstName" : firstName ,
            "lastName" : lastName ,
            "age" : age ,
            "experience" : experience ,
            "motivation" : motivation ,
            "status" : 0
        };

        $( '.suscribe_container_confirmation' ).css({
            'z-index' : 3000 ,
            'opacity' : 1 ,
        });

    }

    if( firstName == '' ){
        $( '#firstName' ).css({
            'border' : '1px solid red'
        });
    }

    if( lastName == '' ){
        $( '#lastName' ).css({
            'border' : '1px solid red'
        });
    }

    if( age < 5 ){
        $( '#age' ).css({
            'border' : '1px solid red'
        });
    }

    if( experience == '' ){
        $( '#experience' ).css({
            'border' : '1px solid red'
        });
    }

    if( motivation == '' ){
        $( '#motivation' ).css({
            'border' : '1px solid red'
        });
    }

});


$( '.suscribe_container_form_right_ctn_form_text' ).focus( function(){
    $( this ).css({
        'border' : '1px solid #0000002f'
    });
});
$( '.suscribe_container_form_right_ctn_form_area' ).focus( function(){
    $( this ).css({
        'border' : '1px solid #0000002f'
    });
});


$( '.confirm' ).click( function(){
    
    xhr.onload = () =>{
        if( xhr.status == 201 ){
            const res = JSON.parse( xhr.responseText ) ;
            $( '.suscribe_container_confirmation' ).css({
                'z-index' : -3000 ,
                'opacity' : 0 ,
            });

            $( '.suscribe_container_loadGif' ).css({
                'z-index' : -3000 ,
                'opacity' : 0 ,
            });

            $( '.suscribe_container_form_right_ctn_form_text' ).val( '' ) ;
            $( '.suscribe_container_form_right_ctn_form_area' ).val( '' ) ;

            alert( "Send successfully" ) ;
        }

        else{
            alert( "Failed to send" ) ;

            $( '.suscribe_container_loadGif' ).css({
                'z-index' : -3000 ,
                'opacity' : 0 ,
            });
        }
    }
    $( '.suscribe_container_loadGif' ).css({
        'z-index' : 3000 ,
        'opacity' : 0.5 ,
    });
    xhr.open( 'POST' , 'https://dojovone.herokuapp.com/submission/create' ) ;
    xhr.setRequestHeader( "Content-Type" , "application/json" ) ;
    xhr.send( JSON.stringify( data ) ) ;

});

$( '.cancel' ).click( function(){
    $( '.suscribe_container_confirmation' ).css({
        'z-index' : -3000 ,
        'opacity' : 0 ,
    });
});
var data ;
var xhr = new XMLHttpRequest() ;
xhr.withCredentials = true;

$( '#loginSubmit' ).click( function(){
    let user_name = $( '#userName').val() ;
    let password = $( '#password' ).val() ;
    if(  user_name == '' ){
        $( '#userName').css({
            'border' : '1px solid red',
            'color' : 'red',
        });

        $( '#userName' ).val( 'Please enter your username' ) ;
    }

    if(  password == '' ){
        $( '#password').css({
            'border' : '1px solid red',
            'color' : 'red'
        });

        $( '#password' ).attr( 'type' , 'text' ) ;
        $( '#password' ).val( 'Please enter your password' ) ;
    }


    if( user_name != '' && password != '' && $( '#userName' ).css( 'color' ) != 'rgb(255, 0, 0)' && $( '#password' ).css( 'color' ) != 'rgb(255, 0, 0)' ){
        data = {
            "user_name" : user_name ,
            "password" : password ,
        };

        $( '.suscribe_container_loadGif' ).css({
            'opacity' : 0.5 ,
            'z-index' : 3000
        });


        xhr.onload = () =>{
            if( xhr.status == 200 ){
                const res = JSON.parse( xhr.responseText ) ;
                //console.log( res.token ) ;
                window.location = "https://dojovone.herokuapp.com/dashboard" ;

                $( '.suscribe_container_loadGif' ).css({
                    'opacity' : 0 ,
                    'z-index' : -3000
                });
            }
    
            else{
                const res = JSON.parse( xhr.responseText ) ;
                if( res.message == 'Username or password is not valid !' ){
                    $( '.loginEror' ).css({
                        'opacity' : 1
                    });
                }

                $( '.suscribe_container_loadGif' ).css({
                    'opacity' : 0 ,
                    'z-index' : -3000
                });
            }
        }

        xhr.open( 'POST' , 'https://dojovone.herokuapp.com/admin/login_auth' ) ;
        xhr.setRequestHeader( "Content-Type" , "application/json" ) ;
        xhr.send( JSON.stringify( data ) ) ;
    }
});


$( '.suscribe_container_form_right_ctn_form_text' ).focus( function(){
    if( $( this ).css( 'color' ) == 'rgb(255, 0, 0)' ){
    
        $( this ).css({
            'border' : '1px solid #0000002f',
            'color' : 'rgb(1, 12, 36)'
        });

        if( $( this ).attr( 'id' ) == 'userName' ){
            $( this ).val( '' ) ;
        }

        else if( $( this ).attr( 'id' ) == 'password' ){
            $( this ).val( '' ) ;
            $( this ).attr( 'type' , 'password' ) ;
        }

        $( '.loginEror' ).css({
            'opacity' : 0
        });
    }
});
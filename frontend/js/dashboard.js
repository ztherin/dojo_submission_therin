function addSubmissionWaitingPartiel( id , name , motivation , created , updated ){

    var printerAllList = $( '.dashboard_right_ctn_body_getAll_list' ) ;
    
    this.submission = $( '<li/>' ) ;
    this.submission.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm' ) ;
    printerAllList.append( this.submission ) ;

    this.submission_name = $( '<div/>' ) ;
    this.submission_name.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_name wait') ;
    this.submission_name.text( name ) ;
    this.submission.append( this.submission_name ) ;

    this.submission_motivation = $( '<div/>' ) ;
    this.submission_motivation.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_motivation' ) ;
    this.submission.append( this.submission_motivation ) ;

    this.submission_motivation_text = $( '<span/>' ) ;
    this.submission_motivation_text.text( motivation ) ;
    this.submission_motivation.append( this.submission_motivation_text ) ;

    this.submission_created = $( '<div/>' ) ;
    this.submission_created.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_created' ) ;
    this.submission_created.text( 'Created : ' + created ) ;
    this.submission.append( this.submission_created ) ;

    this.submission_updated = $( '<div/>' ) ;
    this.submission_updated.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_updated' ) ;
    this.submission_updated.text( 'Updated : ' + updated ) ;
    this.submission.append( this.submission_updated ) ;

    this.submission_showMore = $( '<div/>' ) ;
    this.submission_showMore.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_more' ) ;
    this.submission.append( this.submission_showMore ) ;

    this.submission_showMore_link = $( '<a/>' ) ;
    this.submission_showMore_link.attr( 'href' , '#' ) ;
    this.submission_showMore_link.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_more_link' ) ;
    this.submission_showMore_link.text( "Show more" ) ;
    this.submission_showMore.append( this.submission_showMore_link ) ;

    this.submission_showMore_link.click( function(){
        new showMore_link( id ) ;
    });

    this.submission_confirmButton = $( '<div/>' ) ;
    this.submission_confirmButton.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_button' ) ;
    this.submission.append( this.submission_confirmButton ) ;

    this.submission_confirmButton_confirm = $( '<button/>' ) ;
    this.submission_confirmButton_confirm.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_button_confirm' ) ;
    this.submission_confirmButton_confirm.text( "confirm" ) ;
    this.submission_confirmButton.append( this.submission_confirmButton_confirm ) ;

    this.submission_confirmButton_confirm.click( ()=>{
        updatedSubmissionStatus( id , 1 , this.submission ) ;
    });

    this.submission_confirmButton_rejected = $( '<button/>' ) ;
    this.submission_confirmButton_rejected.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_button_rejected' ) ;
    this.submission_confirmButton_rejected.text( "rejected" ) ;
    this.submission_confirmButton.append( this.submission_confirmButton_rejected ) ;
    
    this.submission_confirmButton_rejected.click( ()=>{
        updatedSubmissionStatus( id , 2 , this.submission ) ;
    });
}

function addSubmissionValidatPartiel( id , name , motivation , created , updated ){

    var printerAllList = $( '.dashboard_right_ctn_body_getAll_list' ) ;
    
    this.submission = $( '<li/>' ) ;
    this.submission.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm' ) ;
    printerAllList.append( this.submission ) ;

    this.submission_name = $( '<div/>' ) ;
    this.submission_name.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_name valid') ;
    this.submission_name.text( name ) ;
    this.submission.append( this.submission_name ) ;

    this.submission_motivation = $( '<div/>' ) ;
    this.submission_motivation.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_motivation' ) ;
    this.submission.append( this.submission_motivation ) ;

    this.submission_motivation_text = $( '<span/>' ) ;
    this.submission_motivation_text.text( motivation ) ;
    this.submission_motivation.append( this.submission_motivation_text ) ;

    this.submission_created = $( '<div/>' ) ;
    this.submission_created.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_created' ) ;
    this.submission_created.text( 'Created : ' + created ) ;
    this.submission.append( this.submission_created ) ;

    this.submission_updated = $( '<div/>' ) ;
    this.submission_updated.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_updated' ) ;
    this.submission_updated.text( 'Updated : ' + updated ) ;
    this.submission.append( this.submission_updated ) ;

    this.submission_showMore = $( '<div/>' ) ;
    this.submission_showMore.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_more' ) ;
    this.submission.append( this.submission_showMore ) ;

    this.submission_showMore_link = $( '<a/>' ) ;
    this.submission_showMore_link.attr( 'href' , '#' ) ;
    this.submission_showMore_link.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_more_link' ) ;
    this.submission_showMore_link.text( "Show more" ) ;
    this.submission_showMore.append( this.submission_showMore_link ) ;

    this.submission_showMore_link.click( function(){
        new showMore_link( id ) ;
    });
}

function addSubmissionRejectedPartiel( id , name , motivation , created , updated ){

    var printerAllList = $( '.dashboard_right_ctn_body_getAll_list' ) ;
    
    this.submission = $( '<li/>' ) ;
    this.submission.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm' ) ;
    printerAllList.append( this.submission ) ;

    this.submission_name = $( '<div/>' ) ;
    this.submission_name.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_name reject') ;
    this.submission_name.text( name ) ;
    this.submission.append( this.submission_name ) ;

    this.submission_motivation = $( '<div/>' ) ;
    this.submission_motivation.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_motivation' ) ;
    this.submission.append( this.submission_motivation ) ;

    this.submission_motivation_text = $( '<span/>' ) ;
    this.submission_motivation_text.text( motivation ) ;
    this.submission_motivation.append( this.submission_motivation_text ) ;

    this.submission_created = $( '<div/>' ) ;
    this.submission_created.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_created' ) ;
    this.submission_created.text( 'Created : ' + created ) ;
    this.submission.append( this.submission_created ) ;

    this.submission_updated = $( '<div/>' ) ;
    this.submission_updated.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_updated' ) ;
    this.submission_updated.text( 'Updated : ' + updated ) ;
    this.submission.append( this.submission_updated ) ;

    this.submission_showMore = $( '<div/>' ) ;
    this.submission_showMore.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_more' ) ;
    this.submission.append( this.submission_showMore ) ;

    this.submission_showMore_link = $( '<a/>' ) ;
    this.submission_showMore_link.attr( 'href' , '#' ) ;
    this.submission_showMore_link.attr( 'class' , 'dashboard_right_ctn_body_getAll_list_onsbm_more_link' ) ;
    this.submission_showMore_link.text( "Show more" ) ;
    this.submission_showMore.append( this.submission_showMore_link ) ;

    this.submission_showMore_link.click( function(){
        new showMore_link( id ) ;
    });
}

function showMore_link( id ){
    this.xhr = new XMLHttpRequest() ;
        this.xhr.withCredentials = true ;

        this.xhr.onload = () =>{
            if( this.xhr.status == 200 ){
                const res = JSON.parse( this.xhr.responseText ) ;
    
                $( '.loadGif' ).css({
                    'z-index' : -3000 ,
                    'opacity' : 0 ,
                });
                
                showMoreSubmission( res ) ;
            }
    
            else{
                alert( "Failed" ) ;
    
                $( '.loadGif' ).css({
                    'z-index' : -3000 ,
                    'opacity' : 0 ,
                });
            }
        }
        $( '.loadGif' ).css({
            'z-index' : 3000 ,
            'opacity' : 0.5 ,
        });
        this.xhr.open( 'GET' , 'https://dojovone.herokuapp.com/submission/get/' + id ) ;
        this.xhr.setRequestHeader( "Content-Type" , "application/json" ) ;
        this.xhr.send() ;
}

function showMoreSubmission( subm ){

    var printerOneSumission = $( '.dashboard_right_ctn_body_getOne_body' ) ;
    printerOneSumission.html(
        '<div class="dashboard_right_ctn_body_getOne_body_firstname"><strong> First name </strong>: ' + subm.firstName + '</div><div class="dashboard_right_ctn_body_getOne_body_lastname"><strong> Last name</strong></strong> : ' + subm.lastName + '</div><div class="dashboard_right_ctn_body_getOne_body_age"><strong>Age</strong> : ' + subm.age + '</div><div class="dashboard_right_ctn_body_getOne_body_experience"><strong>Experience</strong> :  ' + subm.experience + '</div><div class="dashboard_right_ctn_body_getOne_body_motivation"><strong>Motivation</strong> : ' + subm.motivation + '</div>'
    );
}

function getGroupSUbmission( status ){
    let xhr = new XMLHttpRequest() ;
    xhr.withCredentials = true ;

    xhr.onload = () =>{
        if( xhr.status == 200 ){
            const res = JSON.parse( xhr.responseText ) ;

            $( '.loadGif' ).css({
                'z-index' : -3000 ,
                'opacity' : 0 ,
            });

            $( '.dashboard_right_ctn_body_getAll_list' ).html( '' ) ;
            
            for( var i = res.length - 1 ; i >= 0 ; i -- ){
                var subm = res[ i ] ;
                if( status == 0 ){
                    new addSubmissionWaitingPartiel( subm._id , subm.firstName + " " + subm.lastName , subm.motivation , subm.created , subm.updated ) ;
                }

                else if( status == 1 ){
                    new addSubmissionValidatPartiel( subm._id , subm.firstName + " " + subm.lastName , subm.motivation , subm.created , subm.updated ) ;
                }

                else if( status == 2 ){
                    new addSubmissionRejectedPartiel( subm._id , subm.firstName + " " + subm.lastName , subm.motivation , subm.created , subm.updated ) ;
                }
            }
        }

        else{
            alert( "Failed" ) ;

            $( '.loadGif' ).css({
                'z-index' : -3000 ,
                'opacity' : 0 ,
            });
        }
    }
    $( '.loadGif' ).css({
        'z-index' : 3000 ,
        'opacity' : 0.5 ,
    });
    xhr.open( 'GET' , 'https://dojovone.herokuapp.com/submission/get/all/' + status ) ;
    xhr.setRequestHeader( "Content-Type" , "application/json" ) ;
    xhr.send() ;
}


function updatedSubmissionStatus( id , status , elt){
    let data = null ;

    if( id && status ){
        data = {
            "status" : status
        };
        
        let xhr = new XMLHttpRequest() ;
        xhr.withCredentials = true ;

        xhr.onload = () =>{
            if( xhr.status == 200 ){
                const res = JSON.parse( xhr.responseText ) ;

                $( '.loadGif' ).css({
                    'z-index' : -3000 ,
                    'opacity' : 0 ,
                });

                elt.remove() ;
            }

            else{
                alert( "Failed" ) ;

                $( '.loadGif' ).css({
                    'z-index' : -3000 ,
                    'opacity' : 0 ,
                });
            }
        }
        $( '.loadGif' ).css({
            'z-index' : 3000 ,
            'opacity' : 0.5 ,
        });
        xhr.open( 'PUT' , 'https://dojovone.herokuapp.com/submission/update/' + id ) ;
        xhr.setRequestHeader( "Content-Type" , "application/json" ) ;
        xhr.send( JSON.stringify( data ) ) ;
    }
}


function countSubmission( clb ){
    let xhr = new XMLHttpRequest() ;
    xhr.withCredentials = true ;

    xhr.onload = () =>{
        if( xhr.status == 200 ){
            const res = JSON.parse( xhr.responseText ) ;
    
            $( '.loadGif' ).css({
                'z-index' : -3000 ,
                'opacity' : 0 ,
            });
                
            clb( res ) ;
        }
    
        else{
            alert( "Failed" ) ;
    
            $( '.loadGif' ).css({
                'z-index' : -3000 ,
                'opacity' : 0 ,
            });
        }
    }
    $( '.loadGif' ).css({
        'z-index' : 3000 ,
        'opacity' : 0.5 ,
    });
    xhr.open( 'GET' , 'https://dojovone.herokuapp.com/submission/counts' ) ;
    xhr.setRequestHeader( "Content-Type" , "application/json" ) ;
    xhr.send() ;
}

Chart.scaleService.updateScaleDefaults('linear', {
    ticks: {
        
    }
});

function graph( tab ){
    this.canvas = $( '<canvas/>' ) ;
    this.canvas.attr( 'id' , 'myChart' ) ;
    this.canvas.attr( 'aria-label' , 'DOJO submission graph' ) ;
    this.canvas.attr( 'role' , 'img' ) ;

    var printerGraoh = $( '.dashboard_right_ctn_body_getOne_body' ) ;
    printerGraoh.html( '' ) ;
    printerGraoh.append( this.canvas ) ;

    this.myChart = new Chart( document.getElementById('myChart').getContext('2d') , {
        type: 'doughnut',
        data: {
            
            datasets: [{
                data : tab ,
    
                backgroundColor : ['silver', 'green', 'red' ] ,
                borderColor : 'transparent' ,
                hoverBackgroundColor : ['silver', 'green', 'red' ] ,
                hoverBorderColor : ['silver', 'green', 'red' ] ,
                hoverBorderWidth : '10' ,
            }],
            labels: [ "WAITING", "VALIDATED", "REJECTED" ] ,
        },
    
    });
}

//new graph(  [ 10, 20, 7 ] ) ;

getGroupSUbmission( 0 ) ;

$( '#submissionWaiting' ).click( function(){
   getGroupSUbmission( 0 ) ;
    $( '#submissionWaiting' ).css({
       'background-color' : 'rgba( 255 , 255 , 255 , 0.3 )'
    });
   
    $( '#submissionValidated' ).css({
        'background-color' : 'rgba( 255 , 255 , 255 , 0 )'
    });
    
    $( '#submissionRejected' ).css({
        'background-color' : 'rgba( 255 , 255 , 255 , 0 )'
    });

});

$( '#submissionValidated' ).click( function(){
    getGroupSUbmission( 1 ) ;

    $( '#submissionWaiting' ).css({
        'background-color' : 'rgba( 255 , 255 , 255 , 0 )'
     });
    
     $( '#submissionValidated' ).css({
         'background-color' : 'rgba( 255 , 255 , 255 , 0.3 )'
     });
     
     $( '#submissionRejected' ).css({
         'background-color' : 'rgba( 255 , 255 , 255 , 0 )'
     });
});

$( '#submissionRejected' ).click( function(){
    getGroupSUbmission( 2 ) ;

    $( '#submissionWaiting' ).css({
        'background-color' : 'rgba( 255 , 255 , 255 , 0 )'
     });
    
     $( '#submissionValidated' ).css({
         'background-color' : 'rgba( 255 , 255 , 255 , 0 )'
     });
     
     $( '#submissionRejected' ).css({
         'background-color' : 'rgba( 255 , 255 , 255 , 0.3 )'
     });
});

var tabCount = [] ;

$( '#printerChart' ).click( function(){
    countSubmission( function( tb ){
        new graph( tb ) ;
    });
});